<?php
return [
    'CHIPER_AES' => env('CHIPER_AES'),
	'SECRET_JWT' => env('SECRET_JWT'),
	'SECRET_AAD_AES' => env('SECRET_AAD_AES'),
    'MAIL_FROM' => env('MAIL_FROM'),
    'APP_NAME' => env('APP_NAME'),
    'ACTUAL_PATH' => env('ACTUAL_PATH'),
    'ASSETS_URL' => env('ASSETS_URL'),
    'APP_URL' => env('APP_URL'),
    'CACHE' => env('CACHE'),
    'CACHE_TIME' => env('CACHE_TIME_IN_MINUTES'),
    'MAX_SIZE_IMAGE_UPLOAD' => env('MAX_SIZE_IMAGE_UPLOAD'),
    'IMAGES_THUMBNAIL' => env('IMAGES_THUMBNAIL'),
    'IMAGES_HEADLINE' => env('IMAGES_HEADLINE'),
    'IMAGES_ORIGINAL' => env('IMAGES_ORIGINAL'),
    'ACTUAL_DIRECTORY' => env('ACTUAL_DIRECTORY')
];
