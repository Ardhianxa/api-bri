<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//NO TOKEN REQUIRED
$router->group(['prefix' => '', 'middleware' => ['before_global','after_global']], function () use ($router) {
	$router->get('/', ['as' => 'root', 'uses' => 'CommonController@generateJwt']);
	$router->get('generatejwt', ['as' => 'generatejwt', 'uses' => 'CommonController@generateJwt']);	

	// $router->post('client/register', ['as' => 'register', 'uses' => 'AuthController@client_register']);
	// $router->post('client/auth/login', ['as' => 'client-login', 'uses' => 'AuthController@login_client']);
	// $router->post('client/auth/validate/user', ['middleware' => 'before_global', 'as' => 'client-auth-validate', 'uses' => 'AuthController@user']);

	//channel [home, channel, headline, tag, popular, search]
	$router->get('article/{type}/{channel}/{key}/{page}', [
		'as' => 'content', 'uses' => 'ArticleController@index'
	]);

	$router->get('read/{article_id}', [
		'as' => 'content', 'uses' => 'ArticleController@read'
	]);

	$router->get('channel/list', [
		'as' => 'content', 'uses' => 'ArticleController@channel'
	]);

	$router->get('flush', [
		'as' => 'flush', 'uses' => 'ArticleController@flush'
	]);
});

$router->group(['prefix' => '', 'middleware' => ['JwtBearer']], function () use ($router) {

});





