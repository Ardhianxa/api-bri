<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use App\Http\Models\Client;
use App\Http\Models\Provider;
use App\Http\Controllers\CommonController;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function getImage($node,$object,$field) 
    {
        $data = new \stdClass();

        //Field
        foreach ($field as $val_field){
            $data->$val_field = $object->$val_field;
        }

        foreach($node as $node_key => $nod_value){
            if($data->$node_key != ""){
                $data->$node_key = env('APP_BASE_URL').env($nod_value).$object->$node_key;
            }
            else{
                $data->$node_key = env('APP_BASE_URL').env('IMAGES_404').'No-image-found.jpg';
            }
        }

        return $data;
    }

    public function provider(Request $request)
    {
        $token = $request->bearerToken();
        $find = Provider::where('status',1)->where('token',$token)->first();
        $find = Self::getImage(
            array(
                "image" => "IMAGES_PROVIDER_PROFILE",
                "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
            ),
            $find,array('id','email','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token'));

        if(isset($find->id)){
            return response()->json([
                'data' => $find,
                'message' => 'Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Provider with this token not found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function user(Request $request)
    {
        $token = $request->bearerToken();
        $find = Client::where('status',1)->where('token',$token)->first();
        
        $find = Self::getImage(
            array(
                "image" => "IMAGES_CLIENT_PROFILE"
            ),
            $find,array('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token'));


        if(isset($find->id)){
            return response()->json([
                'data' => $find,
                'message' => 'Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Client with this token not founds',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function provider_logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek ID
        $provider = Provider::select('id')->where('id',$request->providerId)->first();
        if(!isset($provider->id)){
            return response()->json([
                'message' => 'Provider Not Found',
                'status' => 404,
                'success' => false
            ],200);
        }
        else{
            $provider->token = '';
            $provider->save();

            return response()->json([
                'message' => 'Provider Logout Success',
                'status' => 404,
                'success' => true
            ],200);
        }
    }

    public function client_logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek ID
        $client = Client::select('id')->where('id',$request->clientId)->first();
        if(!isset($client->id)){
            return response()->json([
                'message' => 'Client Not Found',
                'status' => 404,
                'success' => false
            ],200);
        }
        else{
            $client->token = '';
            $client->save();

            return response()->json([
                'message' => 'Client Logout Success',
                'status' => 200,
                'success' => true
            ],200);
        }
    }

    public function login_provider(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek_data
        $find = Provider::select('id')->where('status',1)->where('email',$request->email)->first();
        if(isset($find->id)){

            $token = CommonController::generateJwt($request);

            $updateToken = Provider::find($find->id);
            $updateToken->token = $token;
            $updateToken->save();

            $updateToken = Self::getImage(
            array(
                "image" => "IMAGES_PROVIDER_PROFILE",
                "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
            ),
            $updateToken,array('id','email','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token'));
            
            return response()->json([
                'data' => $updateToken,
                'message' => 'Login Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Provider with this email not found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function login_client(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek_data
        $find = Client::where('status',1)->where('email',$request->email)->first();

        if(isset($find->id)){

            $token = CommonController::generateJwt($request);

            $updateToken = Client::find($find->id);
            $updateToken->token = $token;
            $updateToken->save();

            $updateToken = Self::getImage(
            array(
                "image" => "IMAGES_CLIENT_PROFILE"
            ),
            $updateToken,array('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token'));
            
            return response()->json([
                'data' => $updateToken,
                'message' => 'Login Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Client with this email not found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function provider_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'provider' => 'required',
            'handphone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'status' => 400,
                'success' => false
            ],200);
        }

        //cek_duplicate
        $find = Provider::select('id')->where('email',$request->email)->first();
        if(isset($find->id)){
            return response()->json([
                'message' => 'Provider with this email is already exists',
                'status' => 409,
                'success' => false
            ],200);
        } 

        //create Provider
        $token = CommonController::generateJwt($request);
        $provider = new Provider();
        $provider->email = $request->email;
        $provider->provider = $request->provider;
        $provider->handphone = $request->handphone;
        $provider->phone = $request->phone;
        $provider->fotoDisplay = $request->fotoDisplay;
        $provider->alamat = $request->alamat;
        $provider->kelurahan = $request->kelurahan;
        $provider->kecamatan = $request->kecamatan;
        $provider->kota = $request->kota;
        $provider->provinsi = $request->provinsi;
        $provider->kodepos = $request->kodepos;
        $provider->nomorIzinUsaha = $request->nomorIzinUsaha;
        //$provider->imageNomorIzinUsaha = $request->imageNomorIzinUsaha;
        $provider->deskripsi = $request->deskripsi;
        $provider->status = 1;
        $save = $provider->save();

        if($save){
            return response()->json([
                'data' => $provider,
                'message' => 'Register Success',
                'status' => 200,
                'success' => true
            ],200); 
        }
        else{
            return response()->json([
                'message' => 'Register Failed',
                'status' => 409,
                'success' => false
            ],200); 
        }
    }

    public function client_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'nama' => 'required',
            'handphone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'status' => 400,
                'success' => false
            ],200);
        }

        //cek_duplicate
        $find = Client::select('id')->where('email',$request->email)->first();
        if(isset($find->id)){
            return response()->json([
                'message' => 'Client with this email is already exists',
                'status' => 401,
                'success' => false
            ],200);
        } 

        //create client
        $token = CommonController::generateJwt($request);
        $client = new Client();
        $client->email = $request->email;
        $client->nama = $request->nama;
        $client->handphone = $request->handphone;
        $client->image = $request->image;
        $client->alamat = $request->alamat;
        $client->kelurahan = $request->kelurahan;
        $client->kecamatan = $request->kecamatan;
        $client->kota = $request->kota;
        $client->provinsi = $request->provinsi;
        $client->kodepos = $request->kodepos;
        $client->statusPernikahan = $request->statusPernikahan;
        $client->jumlahAnak = $request->jumlahAnak;
		$client->namaOrangTua = $request->namaOrangTua;
        $client->jumlahAnakLakiLaki = $request->jumlahAnakLakiLaki;
        $client->jumlahAnakPerempuan = $request->jumlahAnakPerempuan;
        // $client->jenisKelamin = $request->jenisKelamin;
        // $client->lat = $request->lat;
        // $client->lon = $request->lon;
        $client->token = $token;
        $client->status = 1;
        $save = $client->save();

        if($save){
            return response()->json([
                'data' => $client,
                'message' => 'Register Success',
                'status' => 200,
                'success' => true
            ],200); 
        }
        else{
            return response()->json([
                'message' => 'Register Failed',
                'status' => 409,
                'success' => false
            ],409); 
        }
    }
}
