<?php

namespace App\Http\Controllers;

use DB;
use Helpers;
use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use App\Http\Models\PremiumLog;
use App\Http\Models\RedeemLog;
use App\Http\Models\Client;
use App\Http\Models\Provider;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redis;

class CommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public static function getFromRedis($keys, $debug = false)
    {
        if ($debug)
            dd(Redis::get($keys));

        return Redis::get($keys);
    }
    
    public static function setToRedis($key, $value, $ttl = null)
    {
        if ($value) {
            if ($ttl == null) {
                $ttl = env('REDIS_TTL', 1) * 300;
            } else {
                $ttl = env('REDIS_TTL', 1) * $ttl;
            }
            try {
                app('redis')->set($key, $value);
                app('redis')->expireat($key, time() + $ttl);
            } catch (\Exception $e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function cekToken($token,$id,$act)
    {
        if($act=="client"){
            $tokenDB = Client::select('token')->where('status',1)->where('id',$id)->first();
        }
        elseif($act=="provider"){
            $tokenDB = Provider::select('token')->where('status',1)->where('id',$id)->first();
        }

        if(isset($tokenDB->token)){
            if($tokenDB->token==$token){ 
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function generateJwt(Request $request){

        $payload = array(
            'iss' => "amaleen",
            'sub' => 1,
            'iat' => time(),
            'exp' => strtotime(date('Y').'-12-31 23:59:59')
        );

        $code = Helpers::generateJwt(json_encode($payload),config('key.secret_jwt'),'HS256');

        return $code;
    }
}