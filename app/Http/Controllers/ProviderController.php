<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use App\Http\Models\Provider;
use App\Http\Models\Layanan;
use App\Http\Models\Banner;
use App\Http\Models\Provider_Layanan;
use App\Http\Models\Provider_Layanan_Produk;
use App\Http\Controllers\CommonController;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Provider_Produk_Image; 

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function productEdit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provideLayananProdukId' => 'required',
            'providerId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek data
        $cek = Provider_Layanan_Produk::select('id')->where('status',1)->where('id',$request->provideLayananProdukId)->first();
        if(!isset($cek->id)){
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data 2',
                'code' => 400
            ],200);
        }

        //save image
        $error = 0;
        if($request->file('image') != ''){
            $base_path = env('ACTUAL_DIRECTORY').env('IMAGES_PROVIDER_PRODUCT');
            $publishdate = time();
            $filename = md5($publishdate).'.'.$request->file('image')->getClientOriginalExtension();
            $upload = $request->file('image')->move($base_path, $filename);
            if(!$upload)
            {
                $error ++;
            }
        }

        if($error==0){
            //save
            $cek->paket = $request->paket;
            $cek->harga = $request->harga;
            $cek->deskripsiSingkat = $request->deskripsiSingkat;
            $cek->deskripsi = $request->deskripsi;
            $cek->status = 1;

            if($cek->save()){

                if($request->file('image') != ''){

                    $images = new Provider_Produk_Image();
                    $images->providerLayananProdukId = $request->provideLayananProdukId;
                    $images->image = $filename;
                    $images->status = 1;
                    $images->save();
                }
                
                return response()->json([
                    'message' => 'Success',
                    'status' => 200,
                    'success' => true
                ],200);
            }
            else{
                return response()->json([
                    'message' => 'Internal Server Error !',
                    'code' => 400
                ],200);
            }
        }
        else{
            return response()->json([
                'message' => 'Internal Server Error 2 !',
                'code' => 400
            ],200);
        }
    }

    public function productDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provideLayananProdukId' => 'required',
            'providerId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek data
        $cek = Provider_Layanan_Produk::select('id')->where('status',1)->where('id',$request->provideLayananProdukId)->first();
        if(!isset($cek->id)){
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data 2',
                'code' => 400
            ],200);
        }

        //delete
        $cek->status = 0;
        if($cek->save()){
            return response()->json([
                'message' => 'Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Internal Server Error !',
                'code' => 400
            ],200);
        }
    }

    public function productAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerLayananId' => 'required',
            'providerId' => 'required',
            'subLayananId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if($request->subLayananId < 7){
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data 2',
                'code' => 400
            ],200);
        }

        //cek data
        $cek = Provider_Layanan::select('id')->where('status',1)->where('id',$request->providerLayananId)->where('providerId',$request->providerId)->where('subLayananId',$request->subLayananId)->first();
        if(!isset($cek->id)){
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data 3',
                'code' => 400
            ],200);
        }

        if($request->subLayananId == 7 || $request->subLayananId == 8){
            $validator = Validator::make($request->all(), [
                'berat' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => 'Invalid Request, Incomplete Data 4',
                    'code' => 400
                ],200);
            }

            $value['berat'] = $request->berat;
        }

        $validator = Validator::make($request->all(), [
            'paket' => 'required',
            'harga' => 'required',
            'deskripsiSingkat' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data 5',
                'code' => 400
            ],200);
        }

        //save image
        $error = 0;
        if($request->file('image') != ''){
            $base_path = env('ACTUAL_DIRECTORY').env('IMAGES_PROVIDER_PRODUCT');
            $publishdate = time();
            $filename = md5($publishdate).'.'.$request->file('image')->getClientOriginalExtension();
            $upload = $request->file('image')->move($base_path, $filename);
            if(!$upload)
            {
                $error ++;
            }
        }
        
        if($error==0){
            //save
            $save = new Provider_Layanan_Produk();
            $save->providerLayananId = $request->providerLayananId;
            $save->paket = $request->paket;
            $save->harga = $request->harga;
            $save->deskripsiSingkat = $request->deskripsiSingkat;
            $save->deskripsi = $request->deskripsi;
            $save->status = 1;

            if($save->save()){

                if($request->file('image') != ''){

                    $images = new Provider_Produk_Image();
                    $images->providerLayananProdukId = $save->id;
                    $images->image = $filename;
                    $images->status = 1;
                    $images->save();
                }

                return response()->json([
                    'message' => 'Success',
                    'status' => 200,
                    'success' => true
                ],200);
            }
            else{
                return response()->json([
                    'message' => 'Internal Server Error !',
                    'code' => 400
                ],200);
            }
        }
        else{
            return response()->json([
                'message' => 'Internal Server Error 2 !',
                'code' => 400
            ],200);
        }
    }

    public function convertJsonToObjectProduct($object, $field) 
    {
        $data   = new \stdClass();

        //Field
        foreach ($field as $val_field) {
            $data->$val_field = $object->$val_field;
        }

        $images = Provider_Produk_Image::select('image')->where('status',1)->where('providerLayananProdukId',$object->id)->get();
        $images = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PRODUCT"
                ),
                $images,array('image'));
        
        $data->images = $images;
        
        
        return $data;
    }

    public function productDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required',
            'productId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $produk = Provider_Layanan_Produk::select('id','paket','berat','deskripsiSingkat','deskripsi','harga')->where('status',1)->where('id',$request->productId)->first();

        if(isset($produk->id)){
            $produk = Self::convertJsonToObjectProduct($produk, array('id','paket','berat','deskripsiSingkat','deskripsi','harga'));
        }
        

        if(isset($produk->id)){
            $success = true;
            $code = 200;
            $produk = $produk;
            $message = "GET Data Product Success";
        }
        else{
            $success = false;
            $code = 400;
            $produk = null;
            $message = "product NOT Found !";
        }

        return response()->json([
            'produk' => $produk,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }

    public function productGet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required',
            'LayananId' => 'required',
            'page' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if($request->page=="" || $request->page==null || $request->page==0){
            return response()->json([
                'message' => 'Page Required',
                'code' => 400
            ],200);
        }

        $page = new \stdClass();
        $perpage = 10;
        $limit = ($perpage * $request->page) - $perpage;

        $produkList = Provider_Layanan_Produk::
        select('provider_layanan_produk.id','paket','berat','deskripsiSingkat','harga')
            ->join('provider_layanan','provider_layanan.id','=','provider_layanan_produk.providerLayananId')
            ->join('sub_layanan','sub_layanan.id','=','provider_layanan.subLayananId')
            ->where('provider_layanan.status',1)->where('provider_layanan_produk.status',1)
            ->where('providerId',$request->providerId)
            ->where('sub_layanan.layananId',$request->LayananId)
            ->get();
        $produkList = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PRODUCT"
                ),
                $produkList,array('id','paket','berat','deskripsiSingkat','harga','image'));
        $produkList_show = collect($produkList)->slice($limit, $perpage)->all();


        $data = count($produkList);
        $count_page = $data / $perpage;
        settype($count_page, "integer");
        $sisa = $data - ($perpage*$count_page);
        if($sisa > 0){
            $count_page = $count_page + 1;
        }
        $last = false;
        if(($request->page * $perpage) >= count($produkList)){
            $last = true;
        }
        $first = true;
        if($request->page > 1){
            $first = false;
        }

        if(count($produkList_show) > 0){
            $page->totalElements = count($produkList);
            $page->totalPages = $count_page;
            $page->last = $last;
            $page->first = $first;
            $page->numberOfElements = count($produkList_show);
            $page->size = $perpage;
            $page->number = $request->page;
        }
        else{
            $page->totalElements = 0;
            $page->totalPages = 0;
            $page->last = false;
            $page->first = false;
            $page->numberOfElements = 0;
            $page->size = 0;
            $page->number = 0;
        }

        return response()->json([
            'content' => $produkList_show,
            'pages' => $page,
            'message' => 'Get Data Product Success',
            'status' => 200,
            'success' => true
        ],200);
    }

    public function serviceGet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $Layanan = Layanan::select('layanan.id','layanan')
        ->join('sub_layanan','sub_layanan.layananId','=','layanan.id')
        ->join('provider_layanan','provider_layanan.subLayananId','=','sub_layanan.id')
        ->where('layanan.status',1)
        ->where('sub_layanan.status',1)
        ->where('provider_layanan.status',1)
        ->where('provider_layanan.providerId',$request->providerId)
        ->groupBy('layanan.id')
        ->get();

        if(isset($Layanan[0]->id)){
            $success = true;
            $code = 200;
            $message = "GET Data Services Success";
        }
        else{
            $success = false;
            $code = 400;
            $banner = null;
            $message = "Services NOT Found !";
        }

        return response()->json([
            'data' => $Layanan,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }

    public function getImage($type,$node,$object,$field) 
    {
        $result = array();
        foreach ($object as $key => $val) {
            $data = new \stdClass();
            foreach ($field as $val_field){
                if(isset($object->id) && $object->id != ''){
                    //tunggal
                    $key_val = $object;
                }
                else{
                    //multiple
                    $key_val = $val;
                }
                $data->$val_field = $key_val->$val_field;
            }

            foreach($node as $node_key => $nod_value){
                if($data->$node_key != ""){
                    $data->$node_key = env('APP_BASE_URL').env($nod_value).$key_val->$node_key;
                }
                else{
                    $data->$node_key = env('APP_BASE_URL').env('IMAGES_404').'No-image-found.jpg';
                }
            }
            $result[] = $data;
        }

        if($type==1){
            return $data;
        }
        elseif($type==2){
            return $result; 
        }
    }

    public function banner(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required',
            'position' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $banner = Banner::select('image','url')->where('status',1)->where('provider',1)->where('position',$request->position)->get();
        if(isset($banner[0]->image)){
            $banner = Self::getImage(2,
                array(
                    "image" => "IMAGES_BANNER"
                ),
                $banner,array('image','url'));

            $success = true;
            $code = 200;
            $message = "GET Data Banner Success";
        }
        else{
            $success = false;
            $code = 400;
            $banner = null;
            $message = "Banner NOT Found !";
        }

        return response()->json([
            'banner' => $banner,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }

    public function upload(Request $request)
    {
        $subLayananId = json_encode(array(1,2));
        $validator = Validator::make($request->all(), [
            'filename' => 'required',
            'providerId' => 'required',
            'subLayananId' => 'required',
            'act' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if(Input::file('filename')!="")
        {
            if($request->act=="profile")
            {
                $path = 'IMAGES_PROVIDER_PROFILE';
                $node = 'image';
            }
            elseif($request->act=="display")
            {
                $path = 'IMAGES_PROVIDER_DISPLAY';
                $node = 'fotoDisplay';
            }

            $base_path = env('ACTUAL_DIRECTORY').env($path);
            $publishdate = time();
            $filename = md5($publishdate).'.'.Input::file('filename')->getClientOriginalExtension();
            $upload = Input::file('filename')->move($base_path, $filename);
            if($upload)
            {
                $update = Provider_Layanan::where('status',1)
                ->where('providerId',$request->providerId)
                ->where('subLayananId',$request->subLayananId)->first();

                if(isset($update->id)){
                    $update->$node = $filename;
                    $update->save();

                    $update = Self::getImage(1,
                    array(
                        "image" => "IMAGES_PROVIDER_PROFILE",
                        "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                    ),
                    $update,array('id','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token'));
                    
                    return response()->json([
                        'data' => $update,
                        'message' => 'Upload Success',
                        'status' => 200,
                        'success' => true
                    ],200);
                }
                else{
                    return response()->json([
                        'message' => 'Data NOT Found',
                        'code' => 401
                    ],200);
                }
            }
            else
            {
                return response()->json([
                    'message' => 'Upload Failed',
                    'code' => 401
                ],200);
            }
        }
    }

    public function setProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required',
            'subLayananId' => 'required',
            'email' => 'required',
            'provider' => 'required',
            'handphone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $provider = Provider_Layanan::join('provider','provider.id','=','provider_layanan.providerId')->select('id')->where('status',1)->where('providerId',$request->providerId)->where('subLayananId',$request->subLayananId)->first();
        if($provider != null){

            $emailDuplicate = Provider::select('id')->where('email',$request->email)->where('id','!=',$request->providerId)->first();
            if(!isset($emailDuplicate->id))
            {
                $provider->email = $request->email;
                $provider->provider = $request->provider;
                $provider->handphone = $request->handphone;
                $provider->phone = $request->phone;
                $provider->alamat = $request->alamat;
                $provider->kelurahan = $request->kelurahan;
                $provider->kecamatan = $request->kecamatan;
                $provider->kota = $request->kota;
                $provider->provinsi = $request->provinsi;
                $provider->kodepos = $request->kodepos;
                $provider->nomorIzinUsaha = $request->nomorIzinUsaha;
                $provider->deskripsi = $request->deskripsi;
                $provider->status = 1;

                $update = $provider->save();
                if($update){

                    $provider = Self::getImage(1,
                    array(
                        "image" => "IMAGES_PROVIDER_PROFILE",
                        "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                    ),
                    $provider,array('id','email','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token'));

                    return response()->json([
                        'data' => $provider,
                        'message' => 'Update Data Provider Success',
                        'status' => 200,
                        'success' => true
                    ],200);
                }
                else{
                    return response()->json([
                        'message' => 'Update Data Provider Failed',
                        'status' => 304,
                        'success' => false
                    ],200);
                }
            }
            else{
                return response()->json([
                    'message' => 'Provider with this email is exists',
                    'status' => 406,
                    'success' => false
                ],200);
            }
        }
        else{
            return response()->json([
                'message' => 'Provider ID Not Found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function getProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'providerId' => 'required',
            'subLayananId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek_data
        $find = Provider_Layanan::
        join('provider','provider.id','=','provider_layanan.providerId')
        ->select('provider_layanan.id','email','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token')
        ->where('provider_layanan.status',1)
        ->where('provider.status',1)
        ->where('providerId',$request->providerId)
        ->where('subLayananId',$request->subLayananId)
        ->first();

        $find = Self::getImage(1,
                array(
                    "image" => "IMAGES_PROVIDER_PROFILE",
                    "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                ),
                $find,array('id','email','provider','handphone','phone','image','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','deskripsi','averageRating','lat','lon','token'));

        if(isset($find->id)){
            return response()->json([
                'data' => $find,
                'message' => 'Get Data Provider Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Provider with this id not found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }
}
