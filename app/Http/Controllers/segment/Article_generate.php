<?php
$page = new \stdClass();
$perpage = 10;
$limit = ($perpage * $request->page) - $perpage;
$content_show = collect($content)->slice($limit)->take($perpage)->values();
$data = count($content);
$count_page = $data / $perpage;
settype($count_page, "integer");
$sisa = $data - ($perpage*$count_page);
if($sisa > 0){
    $count_page = $count_page + 1;
}
$last = false;
if(($request->page * $perpage) >= count($content)){
    $last = true;
}
$first = true;
if($request->page > 1){
    $first = false;
}

if(count($content_show) > 0){
    $page->totalElements = count($content);
    $page->totalPages = $count_page;
    $page->last = $last;
    $page->first = $first;
    $page->numberOfElements = count($content_show);
    $page->size = $perpage;
    $page->number = $request->page;
}
else{
    $page->totalElements = 0;
    $page->totalPages = 0;
    $page->last = false;
    $page->first = false;
    $page->numberOfElements = 0;
    $page->size = 0;
    $page->number = 0;
}

return response()->json([
    'content' => $content_show,
    'pages' => $page,
    'message' => 'Get Data Success',
    'status' => 200,
    'success' => true
],200);
?>