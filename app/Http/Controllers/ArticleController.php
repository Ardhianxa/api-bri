<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\CommonController;

use App\Http\Models\Article;
use App\Http\Models\Channel;
use Cache;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function channel()
    {
        $channel = Channel::list();
        return response()->json([
            'content' => $channel,
            'message' => 'Get Data Success',
            'status' => 200,
            'success' => true
        ],200);
    }

    public function flush()
    {
        Cache::flush();
    }

    public function read(Request $request)
    {
        if($request->article_id=="" || $request->article_id==null || $request->article_id==0){
            return response()->json([
                'message' => 'Page Required',
                'code' => 400
            ],200);
        }

        $article = Article::read($request->article_id);

        return response()->json([
            'content' => $article,
            'message' => 'Get Data Success',
            'status' => 200,
            'success' => true
        ],200);
    }

    public function index(Request $request)
    {
        if($request->page=="" || $request->page==null || $request->page==0 ){
            return response()->json([
                'message' => 'Invalid Request',
                'code' => 400
            ],200);
        }

        if($request->type!="headline" && $request->type!="data"){
            return response()->json([
                'message' => 'Invalid Request',
                'code' => 400
            ],200);
        }

        if($request->channel=="channel" || $request->channel=="popular"){
            //cek channel
            $channel = Channel::select('id')->where('slug',$request->key)->where('status',1)->first();
            if(!isset($channel->id) && $request->key != 'home' && $request->key != 'uptodate' && $request->key != 'popular'){
                return response()->json([
                    'message' => 'Invalid Request',
                    'code' => 400
                ],200);
            }
            if($request->key == 'uptodate' || $request->key == 'home' || $request->key == 'popular'){
                if($request->key=="popular"){
                    $request->key = "popular";
                    $request->channel_id = "popular";
                }
                else{
                    $request->key = "home";
                    $request->channel_id = "home";
                }
            }
            else{
                $request->channel_id = $channel->id;
            }
        }
        elseif($request->type=="tagging" || $request->type=="search"){
            $request->channel_id = $request->key;
        }
        $content = Article::content($request->type,$request->channel_id,$request->page,$request->except,$request->channel,$request->key);
        
        if($request->type=="headline"){
            return include config('constant.ACTUAL_DIRECTORY').'app/Http/Controllers/segment/Article_generate_not_paging.php';
        }
        else{
            return include config('constant.ACTUAL_DIRECTORY').'app/Http/Controllers/segment/Article_generate.php';
        }
    }
}
