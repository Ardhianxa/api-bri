<?php

namespace App\Http\Controllers; 

use Validator;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use App\Http\Models\Client;
use App\Http\Models\Provider;
use App\Http\Models\Provider_Layanan;
use App\Http\Models\Config;
use App\Http\Models\Client_Vote_Provider;
use App\Http\Models\Provider_Layanan_Produk;
use App\Http\Models\Provider_Produk_Image; 
use App\Http\Models\Banner;
use App\Http\Controllers\CommonController;
use Illuminate\Support\Facades\Input;
use DB;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function getImage($type,$node,$object,$field) 
    {
        $result = array();
        foreach ($object as $key => $val) {
            $data = new \stdClass();
            foreach ($field as $val_field){
                if(isset($object->id) && $object->id != ''){
                    //tunggal
                    $key_val = $object;
                }
                else{
                    //multiple
                    $key_val = $val;
                }
                $data->$val_field = $key_val->$val_field;
            }

            foreach($node as $node_key => $nod_value){
                if($data->$node_key != ""){
                    $data->$node_key = env('APP_BASE_URL').env($nod_value).$key_val->$node_key;
                }
                else{
                    $data->$node_key = env('APP_BASE_URL').env('IMAGES_404').'No-image-found.jpg';
                }
            }
            $result[] = $data;
        }

        if($type==1){
            return $data;
        }
        elseif($type==2){
            return $result; 
        }
    }

    public function banner(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'position' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $banner = Banner::select('image','url')->where('status',1)->where('client',1)->where('position',$request->position)->get();


        if(isset($banner[0]->image)){
            $banner = Self::getImage(2,
                array(
                    "image" => "IMAGES_BANNER"
                ),
                $banner,array('image','url'));

            $success = true;
            $code = 200;
            $message = "GET Data Banner Success";
        }
        else{
            $success = false;
            $code = 400;
            $banner = null;
            $message = "Banner NOT Found !";
        }

        return response()->json([
            'banner' => $banner,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }

    public function detail_product(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'productId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $produk = Provider_Layanan_Produk::select('subLayananId','provider_layanan_produk.id','paket','berat','deskripsiSingkat','provider_layanan_produk.deskripsi','harga')
        ->join('provider_layanan','provider_layanan.id','=','provider_layanan_produk.providerLayananId')
        ->where('provider_layanan_produk.status',1)->where('provider_layanan_produk.id',$request->productId)
        ->where('provider_layanan.status',1)
        ->first();

        if(isset($produk->id)){
            
            if($produk->subLayananId==7 || $produk->subLayananId==8)
            {
                //qurban
                $listArray = array('id','paket','berat','deskripsiSingkat','harga','deskripsi');
            }
            elseif($produk->subLayananId==9 || $produk->subLayananId==10)
            {
                //aqiqah
                $listArray = array('id','paket','deskripsiSingkat','harga','deskripsi');
            }

            $produk = Self::convertJsonToObjectProduct($produk, $listArray);
        }
        

        if(isset($produk->id)){
            $success = true;
            $code = 200;
            $produk = $produk;
            $message = "GET Data Product Success";
        }
        else{
            $success = false;
            $code = 400;
            $produk = null;
            $message = "product NOT Found !";
        }

        return response()->json([
            'produk' => $produk,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }

    public function convertJsonToObjectProduct($object, $field) 
    {
        $data   = new \stdClass();

        //Field
        foreach ($field as $val_field) {
            $data->$val_field = $object->$val_field;
        }

        $images = Provider_Produk_Image::select('image')->where('status',1)->where('providerLayananProdukId',$object->id)->get();
        $images = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PRODUCT"
                ),
                $images,array('image'));
        
        $data->images = $images;
        
        
        return $data;
    }

    public function convertJsonToObject($object, $field) 
    {
        $result = array();
        foreach ($object as $key => $val) {
            $data   = new \stdClass();

            //Field
            foreach ($field as $val_field) {
                $data->$val_field = $val->$val_field;
            }

            $image = Provider_Produk_Image::select('image')->where('status',1)->where('providerLayananProdukId',$val->id)->first();
            
            if(isset($image->image)){
            	$data->image = env('APP_BASE_URL').env('IMAGES_PROVIDER_PRODUCT').$image->image;
            }
            else{
            	$data->image = env('APP_BASE_URL').env('IMAGES_404').'No-image-found.jpg';
            }

            $result[] = $data;
        }
        return $result;
    }

    public function detail_provider(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'providerId' => 'required',
            'subLayananId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $provider = Provider::select('provider_layanan.id as providerLayananId','provider.id','provider','handphone','phone','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','nomorIzinUsaha','imageNomorIzinUsaha','deskripsi','averageRating')
        ->join('provider_layanan','provider_layanan.providerId','provider.id')
        ->where('subLayananId',$request->subLayananId)
        ->where('provider.status',1)
        ->where('provider.id',$request->providerId)->first();

        
        if(isset($provider->id)){
            $provider = Self::getImage(1,
                array(
                    "image" => "IMAGES_PROVIDER_PROFILE",
                    "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                ),
                $provider,array('image','providerLayananId','id','provider','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','averageRating'));

            if($request->subLayananId >= 7)
            {
                if($request->subLayananId==7 || $request->subLayananId==8)
                {
                    //qurban
                    $produkList = Provider_Layanan_Produk::select('id','paket','berat','deskripsiSingkat','harga')
                    ->where('status',1)->where('providerLayananId',$provider->providerLayananId)
                    ->get();
                    $produkList = Self::convertJsonToObject($produkList, array('id','paket','berat','deskripsiSingkat','harga'));
                }
                elseif($request->subLayananId==9 || $request->subLayananId==10)
                {
                    //aqiqah
                    $produkList = Provider_Layanan_Produk::select('id','paket','deskripsiSingkat','harga')
                    ->where('status',1)->where('providerLayananId',$provider->providerLayananId)
                    ->get();
                    $produkList = Self::convertJsonToObject($produkList, array('id','paket','deskripsiSingkat','harga'));
                }
                
                $provider->product = $produkList;
            }
            
            
            $provider = $provider;
            $message = "GET Data Provider Success";
            $code = 200;
            $success = true;
        }
        else{
            $data = null;
            $message = "Provider Not Found !";
            $code = 400;
            $success = false;
        }

        return response()->json([
            'data' => $provider,
            'message' => $message,
            'status' => $code,
            'success' => $success
        ],200);
    }
    
    public function vote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'rating' => 'required',
            'providerId' => 'required',
            'layananId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if (!is_numeric($request->rating)) {
            return response()->json([
                'message' => 'Rating Must Be Integer',
                'code' => 400
            ],200);
        }

        $existsVote = Client_Vote_Provider::select('id')->where('status',1)->where('clientId',$request->clientId)->where('providerId',$request->providerId)->first();

        if(isset($existsVote->id)){
            return response()->json([
                'message' => 'You Have Already Voted For This Provider !',
                'code' => 400
            ],200);
        }
        else{
            $save = new Client_Vote_Provider();
            $save->clientId = $request->clientId;
            $save->providerId = $request->providerId;
            $save->layananId = $request->layananId;
            $save->rating = $request->rating;
            $save->status = 1;
            $insert = $save->save();

            if($insert){
                $Provider_Layanan_id = Provider_Layanan::select('id')->where('status',1)->where('providerId',$request->providerId)->where('layananId',$request->layananId)->get();
                
                foreach($Provider_Layanan_id as $Provider_Layanan_id_value)
                {
                    $update = Provider_Layanan::find($Provider_Layanan_id_value->id);
                    $update->totalRating = $update->totalRating + $request->rating;
                    $update->jumlahVotersRating = $update->jumlahVotersRating + 1;
                    $update->averageRating = $update->totalRating / $update->jumlahVotersRating;
                    $update_exe = $update->save();
                }
                
                if($update_exe){
                    return response()->json([
                        'new_rating' => $update->averageRating,
                        'message' => 'Vote Success',
                        'status' => 200,
                        'success' => true
                    ],200);
                }
                else{
                    return response()->json([
                        'message' => 'Save Data Failed',
                        'code' => 500
                    ],200);
                }
            }
            else{
                return response()->json([
                    'message' => 'Save Data Failed',
                    'code' => 500
                ],200);
            }
            
        }
    }

    public function list_product(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'providerId' => 'required',
            'subLayananId' => 'required',
            'page' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if($request->page=="" || $request->page==null || $request->page==0){
            return response()->json([
                'message' => 'Page Required',
                'code' => 400
            ],200);
        }

        $page = new \stdClass();
        $perpage = 10;
        $limit = ($perpage * $request->page) - $perpage;

        $produkList = Provider_Layanan_Produk::select('provider_layanan_produk.id','paket','berat','deskripsiSingkat','harga')
            ->join('provider_layanan','provider_layanan.id','=','provider_layanan_produk.providerLayananId')
            ->where('provider_layanan.status',1)->where('provider_layanan_produk.status',1)->where('providerId',$request->providerId)->where('subLayananId',$request->subLayananId)
            ->get();

        if($request->subLayananId==7 || $request->subLayananId==8)
        {
            //qurban
            $listArray = array('id','paket','berat','deskripsiSingkat','harga');
        }
        elseif($request->subLayananId==9 || $request->subLayananId==10)
        {
            //aqiqah
            $listArray = array('id','paket','deskripsiSingkat','harga');
        }

        $produkList = Self::convertJsonToObject($produkList, $listArray);
        $produkList_show = collect($produkList)->slice($limit, $perpage)->all();


        $data = count($produkList);
        $count_page = $data / $perpage;
        settype($count_page, "integer");
        $sisa = $data - ($perpage*$count_page);
        if($sisa > 0){
            $count_page = $count_page + 1;
        }
        $last = false;
        if(($request->page * $perpage) >= count($produkList)){
            $last = true;
        }
        $first = true;
        if($request->page > 1){
            $first = false;
        }

        if(count($produkList_show) > 0){
            $page->totalElements = count($produkList);
            $page->totalPages = $count_page;
            $page->last = $last;
            $page->first = $first;
            $page->numberOfElements = count($produkList_show);
            $page->size = $perpage;
            $page->number = $request->page;
        }
        else{
            $page->totalElements = 0;
            $page->totalPages = 0;
            $page->last = false;
            $page->first = false;
            $page->numberOfElements = 0;
            $page->size = 0;
            $page->number = 0;
        }

        return response()->json([
            'content' => $produkList_show,
            'pages' => $page,
            'message' => 'Get Data Product Success',
            'status' => 200,
            'success' => true
        ],200);
    }

    public function providers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'layananId' => 'required',
            'filter' => 'required',
            'page' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if($request->layananId != 4){
            if($request->subLayananId == null){
                return response()->json([
                    'message' => 'Sub Layanan ID is required',
                    'code' => 400
                ],200);
            }
        }


        if($request->filter==1){
            if($request->lat==0 || $request->lat==null || $request->lon==0 || $request->lon==""){
                return response()->json([
                    'message' => 'Latitude Langitude Coordinates Client Required',
                    'code' => 400
                ],200);
            }
        }

        $page = new \stdClass();
        if($request->filter==2){

            if($request->page=="" || $request->page==null || $request->page==0){
                return response()->json([
                    'message' => 'Page Required',
                    'code' => 400
                ],200);
            }

            $perpage = 10;
            $limit = ($perpage * $request->page) - $perpage;

            if($request->layananId==4){
                $provider = Provider::
                //select(DB::raw(""))
                select('provider_layanan.id as providerLayananId','provider.id','provider', 'fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','averageRating')
                ->join('provider_layanan','provider_layanan.providerId','=','provider.id')
                ->where('provider.status',1)
                ->where('provider_layanan.status',1)
                ->whereIn('provider_layanan.subLayananId', [9, 10])
                ->orderBy('provider_layanan.averageRating','desc')
                ->orderBy('provider_layanan.provider','asc')
                ->distinct()
                ->get(['p.id']);

                $provider = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PROFILE",
                    "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                ),
                $provider,array('id','provider','fotoDisplay','image','alamat','kelurahan','kecamatan','kota','provinsi','averageRating','providerLayananId'));

                $provider_show = collect($provider)->slice($limit, $perpage)->all();
            }
            else{
                $provider = Provider::
                select('provider_layanan.id as providerLayananId','provider.id','provider','fotoDisplay','alamat','kelurahan','kecamatan','kota','provinsi','averageRating')
                ->join('provider_layanan','provider_layanan.providerId','=','provider.id')
                ->where('provider.status',1)
                ->where('provider_layanan.status',1)
                ->where('provider_layanan.subLayananId',$request->subLayananId)
                ->orderBy('provider_layanan.averageRating','desc')
                ->orderBy('provider_layanan.provider','asc')
                ->get();

                $provider = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PROFILE",
                    "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                ),
                $provider,array('id','provider','fotoDisplay','image','alamat','kelurahan','kecamatan','kota','provinsi','averageRating','providerLayananId'));

                $provider_show = collect($provider)->slice($limit, $perpage)->all();
            }

            $data = count($provider);
            $count_page = $data / $perpage;
            settype($count_page, "integer");
            $sisa = $data - ($perpage*$count_page);
            if($sisa > 0){
                $count_page = $count_page + 1;
            }
            $last = false;
            if(($request->page * $perpage) >= count($provider)){
                $last = true;
            }
            $first = true;
            if($request->page > 1){
                $first = false;
            }

            if(count($provider_show) > 0){
                $page->totalElements = count($provider);
                $page->totalPages = $count_page;
                $page->last = $last;
                $page->first = $first;
                $page->numberOfElements = count($provider_show);
                $page->size = $perpage;
                $page->number = $request->page;
            }
            else{
                $page->totalElements = 0;
                $page->totalPages = 0;
                $page->last = false;
                $page->first = false;
                $page->numberOfElements = 0;
                $page->size = 0;
                $page->number = 0;
            }
        }
        elseif($request->filter==1){
            if($request->layananId==4){
                $sub_query = " and subLayananId in(9,10)";
            }
            else{
                $sub_query = " and subLayananId='$request->subLayananId'";
            }

            $distance = Config::select('content')->where('status',1)->where('id',1)->first()->content; //km
            
            $provider_show = DB::select(DB::raw(
                "
                SELECT distinct p.id,
                (
                    (
                        ACOS(
                            SIN($request->lat * PI() / 180) 
                            * SIN(lat * PI() / 180) 
                            + COS($request->lat * PI() / 180) 
                            * COS(lat * PI() / 180) 
                            * COS(($request->lon - lon) * PI() / 180)
                        ) 
                        * 180 / PI()
                    ) 
                    * 60 
                    * 1.1515
                ) * 1.60934
                AS distance,image,provider,fotoDisplay,alamat,kelurahan,kecamatan,kota,provinsi,averageRating,lat,lon, pl.id as providerLayananId
                FROM provider p 
                join provider_layanan pl on pl.providerId=p.id 
                WHERE 
                pl.status=1 and p.status=1 $sub_query and 
                lat != 0 and lon != 0 
                HAVING distance <= '$distance' ORDER BY distance ASC
                "
            ));

            $provider_show = Self::getImage(2,
                array(
                    "image" => "IMAGES_PROVIDER_PROFILE",
                    "fotoDisplay" => "IMAGES_PROVIDER_DISPLAY"
                ),
                $provider_show,array('id','provider','fotoDisplay','image','alamat','kelurahan','kecamatan','kota','provinsi','averageRating','providerLayananId'));
        }

        return response()->json([
            'content' => $provider_show,
            'pages' => $page,
            'message' => 'Get Data Providers Success',
            'status' => 200,
            'success' => true
        ],200);
    }

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'filename' => 'required',
            'clientId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        if(Input::file('filename')!="")
        {
            $base_path = env('ACTUAL_DIRECTORY').env('IMAGES_CLIENT_PROFILE');
            $publishdate = time();
            $filename = md5($publishdate).'.'.Input::file('filename')->getClientOriginalExtension();
            $upload = Input::file('filename')->move($base_path, $filename);
            if($upload)
            {
                $update = Client::find($request->clientId);
                $update->image = $filename;
                $update->save();

                $find = Client::select('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token')->where('status',1)->where('id',$request->clientId)->first();
                $find = Self::getImage(1,
                        array(
                            "image" => "IMAGES_CLIENT_PROFILE"
                        ),
                        $find,array('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token'));
                
                return response()->json([
                    'data' => $find,
                    'message' => 'Upload Success',
                    'status' => 200,
                    'success' => true
                ],200);
            }
            else
            {
                return response()->json([
                    'message' => 'Upload Failed',
                    'code' => 401
                ],200);
            }
        }
    }

    public function setProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required',
            'email' => 'required',
            'nama' => 'required',
            'handphone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        $update = Client::find($request->clientId);
        if($update != null){

            //cek duplicate with other email
            $emailDuplicate = Client::select('id')->where('email',$request->email)->where('id','!=',$request->clientId)->first();
            if(!isset($emailDuplicate->id))
            {
                $update->email = $request->email;
                $update->nama = $request->nama;
                $update->handphone = $request->handphone;
                $update->alamat = $request->alamat;
                $update->kelurahan = $request->kelurahan;
                $update->kecamatan = $request->kecamatan;
                $update->kota = $request->kota;
                $update->provinsi = $request->provinsi;
                $update->kodepos = $request->kodepos;
				$update->namaOrangTua = $request->namaOrangTua;
                $update->statusPernikahan = $request->statusPernikahan;
				$update->jenisKelamin = $request->jenisKelamin;
                $update->jumlahAnak = $request->jumlahAnak;
            	$update->jumlahAnakLakiLaki = $request->jumlahAnakLakiLaki;
            	$update->jumlahAnakPerempuan = $request->jumlahAnakPerempuan;
                $update->lat = $request->lat;
                $update->lon = $request->lon;
                $save = $update->save();
                if($save){
                	
                    $update = Self::getImage(1,
                        array(
                            "image" => "IMAGES_CLIENT_PROFILE"
                        ),
                        $update,array('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token'));
                

                    return response()->json([
						'data' => $update,
                        'message' => 'Update Data Client Success',
                        'status' => 200,
                        'success' => true
                    ],200);
                }
                else{
                    return response()->json([
                        'message' => 'Update Data Client Failed',
                        'status' => 304,
                        'success' => false
                    ],200);
                }
            }
            else{
                return response()->json([
                    'message' => 'Client with this email is exists',
                    'status' => 406,
                    'success' => false
                ],200);
            }
        }
        else{
            return response()->json([
                'message' => 'Client ID Not Found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }

    public function getProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Request, Incomplete Data',
                'code' => 400
            ],200);
        }

        //cek_data
        $find = Client::select('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token')->where('status',1)->where('id',$request->clientId)->first();
        $find = Self::getImage(1,
                array(
                    "image" => "IMAGES_CLIENT_PROFILE"
                ),
                $find,array('id','email','nama','handphone','image','alamat','kelurahan','kecamatan','kota','provinsi','kodepos','namaOrangTua','statusPernikahan','jumlahAnak','jumlahAnakLakiLaki','jumlahAnakPerempuan','jenisKelamin','lat','lon','token'));
        
        if(isset($find->id)){
            return response()->json([
                'data' =>  $find,
                'message' => 'Get Data Client Success',
                'status' => 200,
                'success' => true
            ],200);
        }
        else{
            return response()->json([
                'message' => 'Client with this id not found',
                'status' => 404,
                'success' => false
            ],200);
        }
    }
}
