<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class File_manager extends Model
{
    protected $connection = 'mysql';
    protected $table = 'file_manager';
    protected $primaryKey = 'id';

    public static function image($id,$type)
    {
        $data = Self::select('filename')->where('id',$id)->first();

        $result = "";
        if(isset($data->filename)){
            if($type=="headline"){
                $result = config('constant.IMAGES_HEADLINE').$data->filename;
            }
            elseif($type=="data"){
                $result = config('constant.IMAGES_THUMBNAIL').$data->filename;
            }
            else{
                $result = config('constant.IMAGES_ORIGINAL').$data->filename;
            }
        }

        return $result;
    }
}
