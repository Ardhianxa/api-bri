<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;
use Carbon\Carbon;
use App\Http\Controllers\CommonController;

class Channel extends Model
{
    protected $connection = 'mysql';
    protected $table = 'channel';
    protected $primaryKey = 'id';

    public static function list()
    {
        if(config('constant.CACHE')){
            $cache_name = "channel";
            $redis_data = CommonController::getFromRedis($cache_name);
            if (!empty($redis_data)) {
                $data = json_decode($redis_data);
            }
            else{
                $data = Self::list_data();
                $save_redis = CommonController::setToRedis($cache_name, json_encode($data, true), 60);
            }
        }
        else{
            $data = Self::list_data();
        }

        return $data;
    }

    public static function list_data()
    {
        return Channel::select('id','name','slug')->where('status',1)->get();
    }
}
