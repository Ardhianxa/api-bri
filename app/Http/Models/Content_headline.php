<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Content_headline extends Model
{
    protected $connection = 'mysql';
    protected $table = 'content_headline';
    protected $primaryKey = 'id';
}
