<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;
use Carbon\Carbon;
use App\Http\Models\File_manager;
use App\Http\Models\Channel;
use App\Http\Models\Content_gallery;
use App\Http\Models\Content_headline;
use App\Http\Controllers\CommonController;

class Article extends Model
{
    protected $connection = 'mysql';
    protected $table = 'article';
    protected $primaryKey = 'id';

    public static function read($article_id)
    {
        if(config('constant.CACHE')){
            $cache_name = "read_".$article_id;
            $redis_data = CommonController::getFromRedis($cache_name);
            if (!empty($redis_data)) {
                $data = json_decode($redis_data);
            }
            else{
                $data = Self::read_data($article_id);
                $save_redis = CommonController::setToRedis($cache_name, json_encode($data, true), 60);
            }
        }
        else{
            $data = Self::read_data($article_id);
        }

        return $data;
    }

    public static function read_data($article_id)
    {
        $data = Article::select('id','title','summary','body','video','comment','channel_id','view');
        $data->with('Image','Channel');
        $data->where('status',1);
        $data->where('id',$article_id);
        $data->where('user_id','!=',null);
        $data->where('publish_date','<=',date('Y-m-d H:i:s'));
        $content = $data->first();

        //update_view
        $content->view = $content->view + 1;
        $content->save();

        if(!empty($content->image)){
            $content->image->transform(function($image){
                $image->image = File_manager::image($image->file_manager_id,"detail");
                return $image;
            });
        }

        return $content;
    }

    public static function content($type,$channel_id,$page,$except,$channel,$key)
    {
        if(config('constant.CACHE')){
            $cache_name = "content_".$type.'_'.$channel_id.'_'.$page;
            $redis_data = CommonController::getFromRedis($cache_name);
            if (!empty($redis_data)) {
                $data = json_decode($redis_data);
            }
            else{
                $data = Self::content_data($type,$channel_id,$page,$except,$channel,$key);
                $save_redis = CommonController::setToRedis($cache_name, json_encode($data, true), 60);
            }
        }
        else{
            $data = Self::content_data($type,$channel_id,$page,$except,$channel,$key);
        }

        return $data;
    }

    public static function content_data($type,$channel_id,$page,$except,$channel,$key)
    {
        $data = Article::
        select('article.id','title','summary');
        $data->where('article.status',1);
        $data->where('user_id','!=',null);
        $data->where('publish_date','<=',date('Y-m-d H:i:s'));
        
        //filter
        if($type=="data"){
            if($channel_id != "home" && $channel_id != "" && $channel_id != "popular"){
                $data->where('channel_id',$channel_id);
            }
            if($channel=="tagging"){
                $data->join('content_tagging','content_tagging.content_id','=','article.id');
                $data->join('tagging','tagging.id','=','content_tagging.tagging_id');
                $data->where('tagging.slug',$key);
            }
            if($channel=="search"){
                $data->whereRaw("
                (
                    (title LIKE '%".$key."%') or 
                    (summary LIKE '%".$key."%') or 
                    (body LIKE '%".$key."%')
                )
                ");
            }

            if($channel=="popular"){
                $data->orderBy('view','desc');
            }
            else{
                $data->orderBy('publish_date','desc');
            }
        }
        elseif($type=="headline"){
            if($channel_id != "home" && $channel_id != "" && $channel_id != "popular"){
                $data->where('channel_id',$channel_id);
            }
            $data->where('is_headline',1);
            $data->skip(0)->take($page);

            if($channel=="tagging"){
                $data->join('content_tagging','content_tagging.content_id','=','article.id');
                $data->join('tagging','tagging.id','=','content_tagging.tagging_id');
                $data->where('tagging.slug',$key);
            }
            if($channel=="search"){
                $data->whereRaw("
                (
                    (title LIKE '%".$key."%') or 
                    (summary LIKE '%".$key."%') or 
                    (body LIKE '%".$key."%')
                )
                ");
            }

            if($channel=="popular"){
                $data->orderBy('view','desc');
            }
            else{
                $data->orderBy('publish_date','desc');
            }
        }

        if(isset($except)){
            $data->skip($except)->take(9223372036854775807);
        }

        $content = $data->get();
        $content->transform(function($content) use ($type){
            $content->image = Content_gallery::get_image($content->id,$type);

            return $content;
        });

        return $content;
    }

    public function Image()
    {
        $data = $this->hasMany('App\Http\Models\Content_gallery', 'content_id', 'id')
            ->select(['id','content_id','file_manager_id'])
            ->orderBy('created_date','asc');

        return $data;
    }

    public function Channel()
    {
        $data = $this->hasMany('App\Http\Models\Channel', 'id', 'channel_id')
            ->select(['id','name','slug']);

        return $data;
    }
}
