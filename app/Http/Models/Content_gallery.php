<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\File_manager;

class Content_gallery extends Model
{
    protected $connection = 'mysql';
    protected $table = 'content_gallery';
    protected $primaryKey = 'id';

    public static function get_image($article_id,$type)
    {
        $file_manager_id = Self::select('file_manager_id')->where('content_id',$article_id)->where('type',1)->first();
        if(isset($file_manager_id->file_manager_id)){
            return File_manager::image($file_manager_id->file_manager_id,$type);
        }
        else{
            return "";
        }
    }
}
