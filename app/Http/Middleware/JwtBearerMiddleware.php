<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use App\Http\Controllers\CommonController;

class JwtBearerMiddleware 
{
    public function handle($request, Closure $next, $guard = null) 
    {
        $token = $request->bearerToken();
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'message' => 'Token not provided.',
                'status' => 401,
                'success' => false
            ],401);
        }

        $url = $_SERVER['REQUEST_URI'];
        $url_explode = explode("/",$url);
        $type = strtolower($url_explode[3]);
        $request->type = $type;

        if($type=="client"){
            $request->id = $request->clientId;
        }
        elseif($type=="provider"){
            $request->id = $request->providerId;
        }

        try {
            $credentials = JWT::decode($token, config('key.secret_jwt'), ['HS256']);
            $request->token = $request->bearerToken();

            $cekToken = CommonController::cekToken($request->token,$request->id,$type);
            if(!$cekToken){
                return response()->json([
                    'message' => 'Invalid Token',
                    'code' => 401
                ],401);
            }

        } catch(ExpiredException $e) {
            return response()->json([
                'message' => 'Provided token is expired.',
                'status' => 401,
                'success' => false
            ],401);
        } catch(\Exception $e) {
            return response()->json([
                'message' => 'An error while decoding token.',
                'status' => 401,
                'success' => false
            ],401);
        }
    
        //$request->request->add(['auth' => ['user'=>$credentials->user, 'desc'=>$credentials->desc]]);
        return $next($request);
    }
}