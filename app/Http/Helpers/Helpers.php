<?php

namespace App\Http\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Auth;

class Helpers
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public static function generateToken($user_id) {
        $payload = array(
            'iss' => "lumen-jwt",
            'sub' => $user_id,
            'iat' => time(),
            'exp' => strtotime(date('Y-m-d').' 23:59:59')
            );
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    public static function generateJwt($payload,$secret_jwt,$algorithm)
    {
        $jwt = JWT::encode(json_decode($payload), $secret_jwt, $algorithm);
        return $jwt;
    }

    public static function activity($request)
    {
        if(!empty(Auth::id()) && $_POST){
            $data = array();
            $data['user_id'] = Auth::id();
            $data['url'] = $request->fullUrl();
            $data['data'] = $request->all();
            $savedata = json_encode($data).'<-->';
            $path = storage_path('logs/access/');

            //folder tahun
            if(!file_exists($path.date('Y').'/'))
            {
                //folder not exists
                mkdir($path.date('Y').'/');
                chmod($path.date('Y').'/',0755);
            }

            //folder bulan
            if(!file_exists($path.date('Y').'/'.date('m').'/'))
            {
                //folder not exists
                mkdir($path.date('Y').'/'.date('m').'/');
                chmod($path.date('Y').'/'.date('m').'/',0755);
            }

            if(!file_exists($path.date('Y').'/'.date('m').'/'.date('d').'.txt'))
            {
                $fp = fopen($path.date('Y').'/'.date('m')."/".date('d').".txt","wb");
                fwrite($fp,$savedata);
                fclose($fp);
                chmod($path.date('Y').'/'.date('m').'/'.date('d').'.txt',0755);
            }
            else
            {
                $insert = file_put_contents($path.date('Y').'/'.date('m').'/'.date('d').'.txt', $savedata.PHP_EOL , FILE_APPEND | LOCK_EX);
            }
        }

        return true;
    }
}
